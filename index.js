var note = document.getElementsByTagName("audio");
var ultimo = null;
var x = 0;

window.addEventListener("keydown", function(e){
    let char = ['Q','2','W','3','E','4','R','T','6','Y','7','U','I','9','O','0','P','+','<','Z','S','X','D','C','V','G','B','H','N','J','M',','];
    Array.from(note).forEach((nota, index) => {
        if(e.key.toUpperCase() === char[index]){
            nota.currentTime = 0;
            nota.play();
        }
    });
});

window.addEventListener("load", ()=>{
    var piano = document.getElementsByClassName("piano");
    var tasti = piano[0].getElementsByTagName("div");
    Array.from(tasti).forEach((tasto, i) => {
        if(ultimo === tasto.className){
            x++;    
        }
        tasto.style = "transform: translate(-"+(i*25-x*25)+"px);";
        ultimo = tasto.className;
        tasto.addEventListener("click", function(){
            note[i].currentTime = 0;
            note[i].play();
        });
    });
});